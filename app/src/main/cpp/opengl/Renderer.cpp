//
// Created by HangoX on 2017/3/23.
//

#include "Renderer.h"

void Renderer::OnSurfaceCreated(jobject instance, jobject gl, jobject config) {

}

void Renderer::OnSurfaceChanged(jobject instance, jobject gl, int32_t width, int32_t height) {

}

void Renderer::OnDrawFrame(jobject instance, jobject gl) {

}

void Renderer::OnSurfaceDestroyed(jobject instance, jobject gl) {

}
