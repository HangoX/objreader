//
// Created by HangoX on 2017/3/23.
//

#ifndef OBJREADER_MYRENDERER_H
#define OBJREADER_MYRENDERER_H


#include "Renderer.h"
#include <GLES3/gl3.h>


class MyRenderer : public Renderer{
public:
    virtual void OnSurfaceCreated(jobject instance, jobject gl, jobject config) override;

    virtual void
    OnSurfaceChanged(jobject instance, jobject gl, int32_t width, int32_t height) override;

    virtual void OnDrawFrame(jobject instance, jobject gl) override;

    virtual void OnSurfaceDestroyed(jobject instance, jobject gl) override;

};


#endif //OBJREADER_MYRENDERER_H
