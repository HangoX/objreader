//
// Created by HangoX on 2017/3/23.
//

#include "MyRenderer.h"

void MyRenderer::OnSurfaceCreated(jobject instance, jobject gl, jobject config) {
    Renderer::OnSurfaceCreated(instance, gl, config);
    glClear(0x00000000);
}

void MyRenderer::OnSurfaceChanged(jobject instance, jobject gl, int32_t width, int32_t height) {
    Renderer::OnSurfaceChanged(instance, gl, width, height);

}

void MyRenderer::OnDrawFrame(jobject instance, jobject gl) {
    Renderer::OnDrawFrame(instance, gl);
}

void MyRenderer::OnSurfaceDestroyed(jobject instance, jobject gl) {
    Renderer::OnSurfaceDestroyed(instance, gl);
}
