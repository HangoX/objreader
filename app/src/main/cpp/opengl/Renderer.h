//
// Created by HangoX on 2017/3/23.
//

#ifndef OBJREADER_RENDERER_H
#define OBJREADER_RENDERER_H


#include <jni.h>
#include <fstream>

class Renderer {
public:
    virtual void OnSurfaceCreated(jobject instance ,jobject gl,jobject config);

    virtual void OnSurfaceChanged(jobject instance ,jobject gl , int32_t width ,int32_t height);

    virtual void OnDrawFrame(jobject instance ,jobject gl);

    virtual void OnSurfaceDestroyed(jobject instance,jobject gl);

};


#endif //OBJREADER_RENDERER_H
