#include <jni.h>
#include <string>
#include <fstream>
#include "loader/tiny_obj_loader.h"
#include "assert.h"
#include "include/Log.h"
#include "loader/loaderTest.h"
#include "opengl/MyRenderer.h"

#define LOG_TAG "JNI-Call"






static Renderer *renderer;

extern "C"
JNIEXPORT void JNICALL
Java_com_hangox_objreader_ObjLoader_loadObjFile(JNIEnv *env, jobject instance, jstring objPath_,
                                                jstring mtlPath_) {
    const char *objPath = env->GetStringUTFChars(objPath_, 0);
    const char *mtlPath = env->GetStringUTFChars(mtlPath_, 0);

    std::string objName = std::string(objPath);
    std::string sMtlPath = std::string(mtlPath);
    MyMesh mesh = loadObj(objName,sMtlPath);

    size_t size = (size_t) (mesh.vertices.size() * 8 * sizeof(float));

    void* buffer = malloc(size);
    jobject directBuffer = env->NewDirectByteBuffer(buffer, size);


    env->ReleaseStringUTFChars(objPath_, objPath);
    env->ReleaseStringUTFChars(mtlPath_, mtlPath);
}


JNIEXPORT jobject JNICALL
Java_com_hangox_objreader_opengl_NativeRenderer_onDrawFrame(JNIEnv *env, jobject instance, jobject gl) {
    renderer->OnDrawFrame(instance,gl);
}

JNIEXPORT jobject JNICALL
Java_com_hangox_objreader_opengl_NativeRenderer_onSurfaceChanged(JNIEnv *env, jobject instance, jobject gl,
                                                          jint width, jint height) {
    renderer->OnSurfaceChanged(instance,gl,width,height);

}

JNIEXPORT jobject JNICALL
Java_com_hangox_objreader_opengl_NativeRenderer_onSurfaceCreated(JNIEnv *env, jobject instance, jobject gl,
                                                          jobject config) {
    renderer = new  MyRenderer();
    renderer->OnSurfaceCreated(instance,gl,config);

}


JNIEXPORT jobject JNICALL
Java_com_hangox_objreader_opengl_NativeRenderer_onSurfaceDestroyed(JNIEnv *env, jobject instance,
                                                            jobject gl) {
    renderer->OnSurfaceDestroyed(instance,gl);
    delete renderer;
    // TODO

}


JNIEXPORT void JNICALL
Java_com_hangox_objreader_opengl_NativeRenderer_config(JNIEnv *env, jobject instance,
                                                       jobjectArray renderer) {


}