package com.hangox.objreader.opengl;

import android.content.Context;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created With Android Studio
 * User hangox
 * Date 2017/3/23
 * Time 下午3:18
 */

public class NativeRenderer implements GLTextureView.Renderer {
    private Context mContext;



    public NativeRenderer(Context context) {
        mContext = context;
    }

    static {
        System.loadLibrary("native-lib");
    }

    public native void config(String[] renderer);


    @Override
    public native void onSurfaceCreated(GL10 gl, EGLConfig config);

    @Override
    public native void onSurfaceChanged(GL10 gl, int width, int height);

    @Override
    public native void onDrawFrame(GL10 gl);

    @Override
    public native void onSurfaceDestroyed(GL10 gl);
}
