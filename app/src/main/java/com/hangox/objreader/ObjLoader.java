package com.hangox.objreader;

/**
 * Created With Android Studio
 * User hangox
 * Date 2017/3/11
 * Time 下午3:13
 */

public class ObjLoader {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    public native void loadObjFile(String objPath, String mtlPath);
}
