package com.hangox.objreader;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hangox.objreader.databinding.ActivityMainBinding;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import hugo.weaving.DebugLog;

public class MainActivity extends AppCompatActivity {

    private final String[] mNeedCopyFile = {
            "gold rose.obj",
            "gold rose.mtl",
    };


    ActivityMainBinding mBinding;

    private String mTargetDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
//        mBinding.texture.setRenderer(new NativeRenderer(this));
        new Thread() {
            @Override
            public void run() {
                copyFileToSDCard();
                loadObj();
            }
        }.start();


    }

    @DebugLog
    private void copyFileToSDCard() {
        mTargetDir = getExternalCacheDir().getAbsolutePath() + "/model";
        File dir = new File(mTargetDir);
        if (!dir.exists()) dir.mkdirs();
        char[] buffer = new char[Integer.MAX_VALUE / 100000];
        for (int i = 0; i < mNeedCopyFile.length; i++) {
            File file = new File(mTargetDir + "/" + mNeedCopyFile[i]);
            if (file.exists()) continue;

            try {
                file.createNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("model/" + mNeedCopyFile[i])));
                while (reader.read(buffer) != -1) {
                    writer.write(buffer);
                    writer.flush();
                }
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @DebugLog
    private String loadObj() {
        AssetManager manager = getAssets();
        new ObjLoader().loadObjFile(getExternalCacheDir().getAbsolutePath() + "/model/" + mNeedCopyFile[0],
                getExternalCacheDir().getAbsolutePath() + "/model/" + mNeedCopyFile[1]);
//            Loader.loadOBJ(manager.open("model/"+mNeedCopyFile[0]), manager.open("model/"+mNeedCopyFile[1]), 1);
        return "";
    }


}
